module git.autistici.org/ai3/tools/acmeserver

go 1.14

require (
	git.autistici.org/ai3/go-common v0.0.0-20210118064555-73f00db54723
	git.autistici.org/ai3/tools/replds v0.0.0-20210117165138-e6368d266143
	github.com/miekg/dns v1.1.40
	github.com/prometheus/client_golang v1.11.0
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83
	gopkg.in/yaml.v2 v2.4.0
)
