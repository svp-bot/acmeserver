package replds

import (
	"context"
	"errors"
	"log"
	"math/rand"
	"strings"
	"sync"
	"time"

	"git.autistici.org/ai3/go-common/clientutil"
	"github.com/prometheus/client_golang/prometheus"
)

var (
	// Maximum size of node values in a single InternalGetNodesResponse,
	// used to avoid excessively large JSON replies.
	maxResponseSize uint64 = 64 * 1024

	// How often to exchange data with peers.
	pollPeriod = 120 * time.Second

	// Timeout for InternalGetNodes requests.
	getNodesTimeout = 120 * time.Second
)

// Node is an annotated path/value entry.
type Node struct {
	Path  string `json:"path"`
	Value []byte `json:"value"`

	Timestamp time.Time `json:"timestamp"`

	Deleted bool `json:"deleted,omitempty"`
}

func (n *Node) Copy() *Node {
	return &Node{
		Path:      n.Path,
		Value:     n.Value,
		Timestamp: n.Timestamp,
		Deleted:   n.Deleted,
	}
}

func (n *Node) withoutValue() *Node {
	return &Node{
		Path:      n.Path,
		Timestamp: n.Timestamp,
		Deleted:   n.Deleted,
	}
}

func (n *Node) metadataOnly() *Node {
	return &Node{
		Path:      n.Path,
		Timestamp: n.Timestamp,
	}
}

type internalGetNodesRequest struct {
	Nodes []*Node `json:"nodes"`
}

type internalGetNodesResponse struct {
	Nodes   []*Node `json:"nodes"`
	Partial bool    `json:"partial,omitempty"`
}

type internalUpdateNodesRequest struct {
	Nodes []*Node `json:"nodes"`
}

// SetNodesRequest is the request type for the SetNodes method.
type SetNodesRequest struct {
	Nodes []*Node `json:"nodes"`
}

// SetNodesResponse is the response returned by the SetNodes method.
type SetNodesResponse struct {
	HostsOk  int `json:"hosts_ok"`
	HostsErr int `json:"hosts_err"`
}

var errTooOld = errors.New("a more recent value exists for this key")

type storage interface {
	getAllNodes() []*Node
	getNodeValue(string) ([]byte, error)
	setNode(*Node) error
}

// Server for the replicated filesync.
type Server struct {
	peers    []string
	network  *network
	storage  storage
	logger   *log.Logger
	readonly bool

	wg    sync.WaitGroup
	stop  chan bool
	mx    sync.Mutex
	nodes map[string]*Node
}

func stripTrailingSlash(peers []string) []string {
	var out []string
	for _, peer := range peers {
		out = append(out, strings.TrimRight(peer, "/"))
	}
	return out
}

// NewServer creates a new Server with the given peers and backends.
func NewServer(peers []string, dir string, tlsConfig *clientutil.TLSClientConfig, readonly bool) (*Server, error) {
	peers = stripTrailingSlash(peers)

	network, err := newNetwork(peers, tlsConfig)
	if err != nil {
		return nil, err
	}

	s := &Server{
		peers:    peers,
		network:  network,
		storage:  newFS(dir),
		stop:     make(chan bool),
		nodes:    make(map[string]*Node),
		readonly: readonly,
	}

	// Do not scan the local filesystem at startup on readonly
	// instances, downloading everything is safer as we do not
	// risk injecting stale data.
	if !readonly {
		for _, node := range s.storage.getAllNodes() {
			s.nodes[node.Path] = node
		}
		if len(s.nodes) > 0 {
			log.Printf("found %d entries in %s", len(s.nodes), dir)
		}
	}

	// The background workers are different if the instance is
	// readonly: normally we run a synchronization worker for each
	// peer, while in the readonly case we periodically poll a
	// different random peer.
	if readonly {
		s.wg.Add(1)
		go func() {
			s.pollRandomPeerThread()
			s.wg.Done()
		}()
	} else {
		for _, peer := range peers {
			s.wg.Add(1)
			go func(peer string) {
				s.pollThread(peer)
				s.wg.Done()
			}(peer)
		}
	}

	return s, nil
}

// Close the server and all its associated resources. Wait for poll
// goroutines to terminate.
func (s *Server) Close() {
	close(s.stop)
	s.wg.Wait()
}

// Allow to switch logger to make tests easier to debug.
func (s *Server) log(fmt string, args ...interface{}) {
	if s.logger != nil {
		s.logger.Printf(fmt, args...)
	} else {
		log.Printf(fmt, args...)
	}
}

// Update one or more nodes as a "transaction": if one update fails,
// the rest are aborted (but previous commits are not reverted, yet).
// Will ping peers with the data when updatePeers is true.
func (s *Server) doSetNodes(ctx context.Context, nodes []*Node, updatePeers bool) (*SetNodesResponse, error) {
	// Update local state.
	for _, node := range nodes {
		cur, ok := s.nodes[node.Path]
		if ok {
			if node.Timestamp.Before(cur.Timestamp) {
				return nil, errTooOld
			} else if node.Timestamp == cur.Timestamp {
				// This can happen because the lock is not kept
				// in pollPeer between the generation of the
				// node metadata and the internal call to
				// InternalUpdateNodes. It is not an error.
				s.log("de-duplicated update of %s", node.Path)
				continue
			}
		}
		if err := s.storage.setNode(node); err != nil {
			return nil, err
		}
		// Keep the value out of our in-memory node map.
		s.nodes[node.Path] = node.withoutValue()
		s.log("received %s @%d", node.Path, node.Timestamp.Unix())
	}

	// Update the global max_seen_timestamp value.
	updateMaxTimestamp(nodes)

	var res SetNodesResponse
	if updatePeers {
		// Ping remote nodes with the updated state. This
		// reduces the latency of update propagation (we don't
		// have to wait until the peers poll us).
		res.HostsOk = 1
		for _, peer := range s.peers {
			err := s.network.Client(peer).internalUpdateNodes(ctx, &internalUpdateNodesRequest{
				Nodes: nodes,
			})
			if err != nil {
				log.Printf("error updating peer %s: %v", peer, err)
				res.HostsErr++
			} else {
				res.HostsOk++
			}
		}
	}

	return &res, nil
}

func (s *Server) setNodes(ctx context.Context, req *SetNodesRequest) (*SetNodesResponse, error) {
	s.mx.Lock()
	defer s.mx.Unlock()
	return s.doSetNodes(ctx, req.Nodes, true)
}

func (s *Server) internalUpdateNodes(ctx context.Context, req *internalUpdateNodesRequest) error {
	s.mx.Lock()
	defer s.mx.Unlock()
	_, err := s.doSetNodes(ctx, req.Nodes, false)
	return err
}

func (s *Server) getNodeWithValue(node *Node) (out *Node, err error) {
	out = node.Copy()
	out.Value, err = s.storage.getNodeValue(node.Path)
	return
}

func nodeDiff(reqNodes []*Node, myNodes map[string]*Node, adder func(*Node) bool) ([]string, bool) {
	var missing []string
	tmp := make(map[string]struct{})
	for _, reqNode := range reqNodes {
		tmp[reqNode.Path] = struct{}{}
		node, ok := myNodes[reqNode.Path]
		if ok && reqNode.Timestamp.Before(node.Timestamp) {
			if !adder(node) {
				return nil, true
			}
		} else if !ok {
			// Oh we don't have this one, want it.
			missing = append(missing, reqNode.Path)
		}
	}
	for _, node := range myNodes {
		if _, ok := tmp[node.Path]; ok {
			continue
		}
		//s.log("remote peer does not have %s", node.Path)
		if !adder(node) {
			return nil, true
		}
	}
	return missing, false
}

// Return nodes (including data) that the caller is missing.
func (s *Server) internalGetNodes(ctx context.Context, req *internalGetNodesRequest) (*internalGetNodesResponse, error) {
	s.mx.Lock()
	defer s.mx.Unlock()

	// Scan the input and local nodes, find nodes that need to be updated on
	// the caller side, and those that it is missing altogether.
	var missing []string
	var resp internalGetNodesResponse
	var totalSize uint64

	// Add a new node to the response, returns false if the response grew
	// too large and we should stop adding results to it. We add at least
	// one element to the response, regardless of its size, to ensure
	// progress.
	add := func(nodeptr *Node) bool {
		nodeWithData, err := s.getNodeWithValue(nodeptr)
		if err != nil {
			s.log("missing value for %s: %v", nodeptr.Path, err)
			return true
		}
		sz := uint64(len(nodeWithData.Value))
		if len(resp.Nodes) > 0 && totalSize+sz > maxResponseSize {
			return false
		}
		totalSize += sz
		resp.Nodes = append(resp.Nodes, nodeWithData)
		return true
	}

	missing, partial := nodeDiff(req.Nodes, s.nodes, add)

	if len(missing) > 0 {
		// We just log this but don't need to do anything, the
		// next call to pollPeer will fetch the missing nodes.
		s.log("we are missing the following nodes: %s", strings.Join(missing, ", "))
	}
	if partial {
		s.log("value size too large, sending partial response")
		resp.Partial = partial
	}
	return &resp, nil
}

func (s *Server) backgroundThread(fn func()) {
	fn()

	tick := time.NewTicker(pollPeriod)
	defer tick.Stop()
	for {
		select {
		case <-s.stop:
			return
		case <-tick.C:
			fn()
		}
	}
}

func (s *Server) pollThread(peer string) {
	s.backgroundThread(func() {
		if err := s.pollPeer(peer); err != nil {
			s.log("error polling peer %s: %v", peer, err)
		}
	})
}

func (s *Server) pollRandomPeerThread() {
	s.backgroundThread(func() {
		if len(s.peers) == 0 {
			s.log("no peers to poll!")
			return
		}
		peer := s.peers[rand.Intn(len(s.peers))]
		if err := s.pollPeer(peer); err != nil {
			s.log("error polling peer %s: %v", peer, err)
		}
	})
}

func (s *Server) pollPeer(peer string) error {
	for {
		partial, err := s.pollPeerRequest(peer)
		if err != nil {
			return err
		}
		if !partial {
			return nil
		}
	}
}

var (
	peerRequests = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "peer_sync_requests",
			Help: "Total number of peer sync requests.",
		},
		[]string{"peer"},
	)
	peerErrors = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "peer_sync_errors",
			Help: "Total number of failed peer sync requests.",
		},
		[]string{"peer"},
	)
	maxTimestamp = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Name: "max_seen_timestamp",
			Help: "Latest timestamp of modified files.",
		},
	)

	// Does not need a lock because we're calling setMaxTimestamp
	// when holding the Server.mx mutex (a bit fragile).
	maxTSValue int64
)

func updateMaxTimestamp(nodes []*Node) {
	var max int64
	for _, node := range nodes {
		t := node.Timestamp.Unix()
		if t > max {
			max = t
		}
	}
	if max > maxTSValue {
		maxTSValue = max
		maxTimestamp.Set(float64(max))
	}
}

func init() {
	prometheus.MustRegister(peerRequests, peerErrors, maxTimestamp)
}

func (s *Server) pollPeerRequest(peer string) (bool, error) {
	peerName := stripHTTP(peer)
	peerRequests.With(prometheus.Labels{"peer": peerName}).Inc()

	// Create a shallow copy of the list of the Nodes we know
	// about, containing just path/timestamp, to send to the peer
	// in our GetNodes request.
	s.mx.Lock()
	var req internalGetNodesRequest
	for _, node := range s.nodes {
		// Create a copy of the Node with just the metadata
		// used by the sync process.
		req.Nodes = append(req.Nodes, node.metadataOnly())
	}
	s.mx.Unlock()

	ctx, cancel := context.WithTimeout(context.Background(), getNodesTimeout)
	defer cancel()

	resp, err := s.network.Client(peer).internalGetNodes(ctx, &req)
	if err != nil {
		peerErrors.With(prometheus.Labels{"peer": peerName}).Inc()
		return false, err
	}

	if len(resp.Nodes) == 0 {
		// Nothing to do.
		s.log("in sync with %s", peer)
		return false, nil
	}

	// Update the internal state with the new data.
	return resp.Partial, s.internalUpdateNodes(ctx, &internalUpdateNodesRequest{Nodes: resp.Nodes})
}

func stripHTTP(uri string) string {
	return strings.TrimPrefix(strings.TrimPrefix(uri, "https://"), "http://")
}
