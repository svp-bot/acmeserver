module git.autistici.org/ai3/tools/replds

go 1.14

require (
	git.autistici.org/ai3/go-common v0.0.0-20210110180225-a05c683cfe23
	github.com/coreos/go-systemd/v22 v22.1.0
	github.com/google/subcommands v1.2.0
	github.com/prometheus/client_golang v1.9.0
	gopkg.in/yaml.v2 v2.4.0
)
